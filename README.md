
# Abstract

This artifact provides a preliminary release of `StreamProf`, a dedicated stream profiler for the JVM. The artifact consists of a ready-to-use Docker image that enables the production of the tables of the related paper (excluding appendices) "*Profiling and Optimizing Java Streams*'', (a) with pre-collected data, and (b) via execution of `StreamProf` on all the stream-based workloads of the [`Renaissance` benchmark suite](https://renaissance.dev/), namely `mnemonics`, `par-mnemonics`, and `scrabble` along with the optimized versions `mnemonics opt1`, `par-mnemonics opt1`, and `par-mnemonics opt2`.

With the goal of easing the evaluation of the artifact by the reviewers, we made a big effort to port our profiler to a containerized environment. Nonetheless, the use of containerization has a significant impact on our measurements, particularly on those based on reference cycles and execution time. We detail the claims that are supported by the artifact in the corresponding section below. During the kick-the-tires phase, we can also give instructions to guide the (rather complex) installation of our profiler and its requirements on a machine running Linux, if desired by the reviewers.

# Getting Started Guide

## Requirements

- A host with at least 16GB of RAM, at least 30GB of free space, bash script support, and an Internet connection.

- A host where the `PAPI_REF_CYC` event is available to [PAPI](https://icl.utk.edu/papi/).

  - The container installs and uses PAPI `6.0.0.1` (the version used in the paper).

 - Docker installed.

- We have tested Docker version `20.10.14, build a224086` on Ubuntu 20.04.4 LTS

## Setting up

1. Extract the artifact tar ball available in the following URL: [https://gitlab.com/eerosales/streamprof-artifact](https://gitlab.com/eerosales/streamprof-artifact) which hash sha256 is `25d46a4c595684e7022d1ccbef50af7aa60811ea4107eea885b8dbc0c1d15651  streamprof-artifact.tar.gz`. It includes the ready-to-use `StreamProf` evaluation Docker image, a script to run the artifact, and a copy of this README file.

## Running StreamProf

- The main script to run `StreamProf` is `run-streamprof.sh`, which receives one of the following arguments specifying the run mode:

- `precollected` : Generates the tables shown in the paper using pre-collected data.

- `profiling` : Runs the profiler in the container to collect data and then produces the tables from the collected data.

## Generating tables with pre-collected data

To generate the tables using pre-collected data from `StreamProf` traces (containing the same results that appear in the paper), follow the instructions below:

1. Execute `run-streamprof.sh` specifying `precollected` as argument.

```bash
$ ./run-streamprof.sh precollected
```

2. After approximately 5 minutes, the generated tables will be integrated in a single PDF: `(-v) output/artifact-precollected.pdf`.

*Note*: The `(-v)` notation before `output` denotes that such a folder is mapped to the `output/` folder in the current path of the host machine. Therefore, you can open files generated in `(-v) output` in the host machine, from outside the Docker container.

## Generating tables with newly profiled data

To generate the tables using newly collected data from `StreamProf` traces, follow the instructions below:

1. Execute `run-streamprof.sh` specifying `profiling test` as arguments.

```bash
$ ./run-streamprof.sh profiling test
```

This mode runs a few measurements (i.e., 2 runs), using `StreamProf` on `mnemonics` and `mnemonics opt1` to validate the experiment settings.

2. After approximately 25 minutes, the generated tables will be integrated in a single PDF: `(-v) output/artifact-test.pdf`.

# Overview of Claims

## Claims not supported by the artifact

- We expect that the exact values reported in Tables 1, 2, 3, and 4, differ from the values presented in the paper. As stated in Section 5, *”our evaluation results may not be valid in other platforms”* and *“the profiles produced using our technique are platform-dependent”*.

- A small number of runs can produce different results from the ones obtained while running 100 runs (as it is done for the evaluation presented in the paper). As stated in Section 5, *”cycle measurements are subjected to variability”*, therefore a significant number of runs is required to compute more accurate results.

- The use of containerization introduces overheads that are not considered in our original setting, which may bias the speedup, accuracy, and overhead computations. In particular, we have seen that workloads executing parallel streams (i.e., `par-mnemonics`, `par-mnemonics opt1`, `par-mnemonics opt2`, and `scrabble`) may experience high contention while being executed in a containerized environment, biasing the reported measurements.

- We have observed that the performance of `par-mnemonics opt2` notably decreases when using containerization. Moreover, the workload might be blocked due to well-known issues compromising liveness in the Java fork-join pool (e.g., [JDK-8281524](https://bugs.openjdk.org/browse/JDK-8161685), [JDK-8161685](https://bugs.openjdk.org/browse/JDK-8281524). However, we have seen that this situation occurs frequently when running the workload in a containerized environment. Our artifact sets a timeout for the execution of this workload, such that the execution of the artifact can proceed even if the workload blocks.

- Tables 3 and 4 in the paper also report the average accuracy and overhead obtained in two additional benchmarks suites, namely the [JEDI benchmark suite](https://github.com/usi-dag/JEDI) and some of the stream-based workloads written by the developers of the OpenJDK and publicly available in the [OpenJDK18 repository](https://github.com/openjdk/jdk18u/tree/master/test/micro/org/openjdk/bench/java/util/stream). We remark that the artifact only produces the tables included in the original submission (excluding any appendix added in the revised version).

## Claims supported by the artifact

- Table 1 reports the speedups enabled in the optimized versions of `mnemonics` and `par-mnemonics` (see Section 3.4 in the paper for details) when executed using the `profiling` mode with the arguments`short` or `full` (as explained in the next section). Despite the execution time is platform dependent and is also affected by containerization-related overheads, we expect that each optimized version of the analyzed workloads (i.e., `mnemonics-opt1`, `par-mnemonics opt1` and `par-mnemonics opt2`) reports a speedup w.r.t. the original version.

- Table 2 reports the four calibration constants (see Section 4.2.1) required for the accuracy computation (Section 4.2.3). Although the constants are platform dependent, we expect that these values remain in the same order of magnitude.

- Table 3 reports the results of the accuracy evaluation (see Section 4.2.3). Although reference cycles are platform dependent and their measurement is affected by the use of containerization, we expect that the accuracy is higher for the optimized versions of the workloads w.r.t. the original versions.

- Table 4 reports the results of our overhead evaluation (see Section 4.3). Although execution times are platform dependent and the use containerization biases the measurements, we expect that the profiling overhead is higher for the original versions of the workloads w.r.t. the optimized versions.

# Step-by-Step Instructions

In this section, we focus on detailed instructions to run the artifact. We provide 3 options for running measurements:

- `test`: This is the test mode explained above, executing the profiler on a small dataset to test the artifact.

- `short` : Runs the evaluation presented in the paper using a small dataset and generates the tables. Given the large amount of time needed to produce the 'full' measurements (see below), we provide this setting to approximate the results obtained in the paper by running `StreamProf` on a much smaller dataset (i.e., 5 runs instead of 100 runs).

1. Execute `run-streamprof.sh` specifying `profiling short` as argument.

```bash
$ ./run-streamprof.sh profiling short
```

2. After approximately 2 hours, the generated tables will be integrated in a single PDF: `(-v) output/artifact-short.pdf`.

- `full`: Runs `StreamProf`on all the stream-based workloads in `Renaissance` analyzed in our paper, setting 100 runs. However, we expect that executing this setting will take a total execution time in the order of days, as each workload is executed a number of warmup iterations before reaching the steady state in which measurements are collected (see Section 3.1.1).

1. Execute `run-streamprof.sh` specifying `profiling full` as argument.

```bash
$ ./run-streamprof.sh profiling full
```

2. After the measurements are completed, the generated tables will be integrated in a single PDF: `(-v) output/artifact-full.pdf`.
  
# Troubleshooting

- Before running the profiling mode, our scripts check whether PAPI can run properly within the container. If you get the following message:

```
Checking dependencies...
Checking docker service status...
Loading Streamprof Docker image... (this may take several minutes)
Loaded image: streamprof-artifact:latest
Checking Hardware Performance Counters and PAPI support...

Fatal error: PAPI could not detect any event.
Please try to execute the following command in your machine: kernel.perf_event_paranoid=-1 > /etc/sysctl.d/local.conf
If the problem persists please check if your kernel or architecture are supported by PAPI.
  ```

If after executing the suggested command (`kernel.perf_event_paranoid=-1 > /etc/sysctl.d/local.conf`), the problem persists, this may indicate that PAPI is not supported by your Linux kernel and/or CPU architecture.

To check whether PAPI 6.0.0.1 is supported in the host machine, you can execute the following commands:

```
wget -q https://icl.utk.edu/projects/papi/downloads/papi-6.0.0.1.tar.gz && \
echo "34c536f3c4a6ad4b5615de23018503ad papi-6.0.0.1.tar.gz" > MD5SUM && \
md5sum -c MD5SUM &&\
tar xf papi-6.0.0.1.tar.gz && \
cd papi-6.0.0.1/src && \
./configure --prefix=$PWD/install && \
make && \
make install

cd /papi-6.0.0.1/src/install/bin

./papi_avail
```
If PAPI 6.0.0.1 is supported by the host machine, some events will be reported as available in the output of the command. The event `PAPI_REF_CYC` must be available for the artifact to run (as stated in our requisites).

- If you have disk space issues with docker, try running `docker system prune` or restart the docker service.

